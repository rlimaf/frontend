$(document).ready(async function() {

  // variáveis
  const storage = firebase.app().storage("gs://big-data-38723.appspot.com");
  const url = await storage.ref().child('output/data.json').getDownloadURL();
  const data = await $.get(url);
  const type = $('#sltType');

  // Gerando o grafico
  function drawGraphic(columns=[], categories=[], chart='bar', rotated=false) {
    c3.generate({
      bindto: "#grafico",
      data: {
        columns,
        type: chart
      },
      bar: {
        width: { ratio: 0.5 }
      },
      axis: {
        rotated,
        x: {
          type: 'category',
          categories: [categories[0]]
        }
      }
    });
  }

  // Muda os tipos
  function changeType(e) {
    e.preventDefault();
    let categories = [];
    let columns = []
    switch (e.target.value) {
      case 'Sexo':
        categories = ['Gêneros'];
         columns = [
          ['Masculino', data.filter(item => item.gender === 'M').reduce((acc, cur) => acc + cur.total, 0)], 
          ['Feminino', data.filter(item => item.gender === 'F').reduce((acc, cur) => acc + cur.total, 0)], 
        ];
        drawGraphic(columns, categories, 'pie');
        break;
      case 'Situação':
        categories = ['Situações'];
        const situactions = data.reduce((acc, cur) => ({ ...acc, [cur.situaction]: acc[cur.situaction] + cur.total || cur.total }), {});
        const keys = Object.keys(situactions);
        columns = keys.map(item => [item, situactions[item]]);
        drawGraphic(columns, categories, 'bar', true);
        break;
      case 'Faixa':
        categories = ['Faixa Etárias'];
        const faixas = data.reduce((acc, cur) => ({ ...acc, [cur.age_range]: acc[cur.age_range] + cur.total || cur.total }), {});
        const header = Object.keys(faixas);
        columns = header.map(item => [item, faixas[item]]);
        drawGraphic(columns, categories);
        break;
      default:
        break;
    }
  }

  // Ações
  type.change(changeType);
  
});
